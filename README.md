# desafio-bry-aplicacoes

## Descrição

Nesse repositório você encontrará o deploy e as aplicações utilizadas para resolver o desafio DevOps da Bry Tecnologia.

Para esse desafio foram utilizadas as ferramentas: [Rancher 1.6](https://rancher.com/docs/rancher/v1.6/en/), [HAProxy](https://rancher.com/docs/rancher/v1.2/en/cattle/adding-load-balancers/), [Prometheus](https://prometheus.io/)  + [Grafana](https://grafana.com/) + [AlertManager](https://prometheus.io/docs/alerting/alertmanager/) e o [Let's Encrypt](https://letsencrypt.org/pt-br/).

Todas elas serão explicadas ao longo deste documento. Caso seja de seu interesse entender como foi realizado o provisionamento da máquina utilizada para o desafio via Terraform ou como a configuração da máquina foi feita via Ansible, favor dirigir-se aos repositórios [desafio-bry-terraform](https://gitlab.com/roberto.rvs/desafio-bry) e [desafio-bry-ansible](https://gitlab.com/roberto.rvs/desafio-bry-ansible).

## Índice
* [Descrição](#descrição)
* [Rancher 1.6](#rancher-1-.-6)
* [whoami](#whoami)
* [letsencrypt](#letsencrypt)
* [monitoramento](#monitoramento)
* [balancer](#balancer)
* [Gitlab CI](#gitlab-ci)


## Rancher 1.6

Optei por utilizar o Rancher 1.6 por inexperiência em kubernetes e por ser a ferramenta utilizada no meu trabalho atual. Sendo assim possuo maior domínio nessa versão.

Após realizar a instalação do Rancher por automatização via Ansible, vamos começar acessando-o para realizar as primeiras configurações. Você pode acessá-lo clicando [aqui](http://devops.desafio-bry.tk:8080).

### Primeiro passo

Adicionaremos o host criado na GCP. Optei por utilizar apenas uma VM pois por algum motivo não identificado, não consegui gerar mais de um ip estático na GCP, o que poderia me atrapalhar ao longo do desafio:


![image](https://i.ibb.co/hdt0PT1/image.png)

Notem que já possuo um Host configurado com o nome ``desafio-bry`` e as labels `whoami=true`, `balancer=true` e `monitoramento=true`. Essas labels são bastante importantes para o rancher identificar em qual host deverá subir as aplicações.

Como no desafio estou utilizando apenas um host, não faz diferença. Porém, por questão de organização, optei por colocá-las mesmo assim. 

Esse host será encarregado de manter todos os containers para o desafio.

### Segundo passo

Tendo o host configurado, nosso segundo passo será gerar as chaves necessárias para autenticação do Gitlab CI via API do Rancher:

![image](https://i.ibb.co/JRh794z/image.png)
![image](https://i.ibb.co/W3dHn1x/image.png)


A Environment API Key (EAK) é composta por duas chaves, sendo elas `Acces Key (Username)` e `Secret Key(Password)`.

Como forma de tornar mais seguro o manuseio da EAK, optei por criar as variáveis protegidas que serão utilizadas pelo CI:

*  **ENV_RANCHER_V1_ACCESS_KEY** : responsável por armazenar a Acces Key
*  **ENV_RANCHER_V1_SECRET_KEY_ENV** : responsável por armazenar a Secret Key
*  **ENV_RANCHER_V1_URL** : responsável por armazenar a URL do Rancher

Finalizando esse passo, podemos dar início ao deploy das stacks que conterão os containers do desafio.

## whoami

Nessa stack encontramos a aplicação ``jwilder/whoami`` definida pelo desafio. Ela consiste em nos mostrar o container id quando o contexto ``/whoami`` é acessado.

Para termos uma maior disponibilidade da aplicação, optei por deixar a escala em 2 containers. Entretanto, o Rancher nos permite escalar quantos containers forem necessários para balancearmos nossas aplicações. Caso tivéssemos dois hosts configurados no Rancher com a label `whoami=true`, cada instância seria subida em um host diferente, intercalando o uso.

Como estamos trabalhando com apenas um host, as duas instâncias subiram na mesma máquina:

![image](https://i.ibb.co/KF9mfbG/image.png)
![image](https://i.ibb.co/0DXg8NM/image.png)

Também configurei o Health Check conforme utilizado no meu trabalho atual para garantir a disponibilidade da aplicação e caso pare de funcionar, gera-se outro container no lugar:

```yml
    health_check:
      response_timeout: 2000
      healthy_threshold: 2
      port: 8000
      unhealthy_threshold: 3
      initializing_timeout: 60000
      interval: 2000
      strategy: recreate
      request_line: GET "/" "HTTP/1.0"
      reinitializing_timeout: 60000
```

No entanto, essa solução de Health Check funciona apenas para requisições ``HTTP``. Desta forma, o ideal será implementar outras abordagens que garantam a saúde da aplicação, como o [Consul](https://www.consul.io/docs/agent/checks.html) por exemplo. 

> Para maiores informações sobre a implementação dessa aplicação, clique [aqui](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/tree/master/whoami).


## letsencrypt

Nessa stack, temos o container do Let's Encrypt que pode ser encontrado no [Rancher Community Catalog](https://github.com/rancher/community-catalog). Esse container é bem simples de configurar e realiza a renovação automática do certificado:

Para realizar a configuração do certificado, foi necessário popular as variáveis do ``letsencrypt-docker-compose``:
*  **CERT_NAME**: desafio-bry-cert
*  **DOMAINS**: devops.desafio-bry.tk

O restante das variáveis podem ficar nulas ou com os valores default pois não nos interessam no momento.

Com o container de pé, é gerado o certificado para o domínio ``devops.desafio-bry.tk``:

![image](https://i.ibb.co/68wy8Df/image.png)

Para validar o certificado, optei por utilizar o ``HTTP Challenge`` junto ao load balancer, adicionando uma entrada para a porta 80 do letsencrypt com o request ``/.well-known/acme-challenge`` que poderá ser verificado mais abaixo.

Tendo os passos acima concluídos, nosso balancer já está preparado para desviar os requests para a porta 443.

> Para maiores informações sobre a implementação dessa aplicação, clique [aqui](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/tree/master/letsencrypt).
Caso você queira entender melhor como funciona as configurações do HTTP Challenge, clique [aqui](https://github.com/janeczku/rancher-letsencrypt).

## monitoramento

### Prometheus

Para o container do Prometheus utilizei a versão mais recente da imagem ``prom/prometheus:latest`` e externalizei o volume ``/prometheus/:/etc/prometheus`` para que ele faça a leitura do arquivo de configuração ``prometheus.yml`` criado por mim contendo as configurações dos jobs e scrapes:

```yml
global:
  scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.

alerting:
  alertmanagers:
  - static_configs:
    - targets:
      - devops.desafio-bry.tk
    path_prefix: /alertmanager
    scheme: https
    timeout: 10s

rule_files:
- /etc/prometheus/prom-rules/*.yml

scrape_configs:
  - job_name: 'node'
    static_configs:
      - targets: ['devops.desafio-bry.tk']
    scheme: https

```

Com essa configuração, é realizado o descobrimento automático do node exporter para a lista de targets. Nesse caso há apenas um target ``devops.desafio-bry.tk``.

Também está configurado o ``Alertmanager`` e onde o Prometheus deverá ler os arquivos de regras/alertas: **- /etc/prometheus/prom-rules/*.yml**

No bloco de código a seguir do serviço prometheus no arquivo [monitoramento-docker-compose.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/monitoramento/monitoramento-docker-compose.yml), utilizei o ``command`` para definir o caminho do arquivo que deverá ser lido para a configuração e o caminho onde deverá permanecer o storage e a url do sistema:

```yml
    command:
    - --config.file=/etc/prometheus/prometheus.yml
    - --storage.tsdb.path=/prometheus
    - --web.external-url=https://devops.desafio-bry.tk/prometheus
```

Para acessar o Prometheus clique em [devops.desafio-bry.tk/prometheus](https://devops.desafio-bry.tk/prometheus).

> Para maiores informações sobre a implementação dessa aplicação, clique [aqui](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/tree/master/monitoramento) e [aqui](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/tree/master/monitoramento/prometheus).


### Node Exporter

Exporter encarregado de expor métricas de memória, CPU, disco e tráfego de rede.

Foi utilizada a versão mais recente da imagem ``prom/node-exporter:latest``.

Ele é identificado automaticamente pelo service discovery do Prometheus. Abaixo seguem algumas métricas expostas:

![image](https://i.ibb.co/0jrnpjW/image.png)

Para acessar as métricas clique em [devops.desafio-bry.tk/metrics](https://devops.desafio-bry.tk/metrics).

### Grafana

Ferramenta capaz de analisar e monitorar aplicações e infraestrutura a partir da criação de dashboards e alertas.

Foi utilizada a versão mais recente da imagem ``grafana/grafana:latest`` e foram externalizados os volumes que comportam os arquivos de configuração ``defaults.ini`` e os arquivos de provisionamento de ``dashboards`` e ``datasources``.

Desta forma, posso utilizar os arquivos manipulados por mim:

```yml
  grafana:
    image: grafana/grafana
    volumes:
    - /grafana/conf/defaults.ini:/usr/share/grafana/conf/defaults.ini:Z
    - /grafana/conf/provisioning:/etc/grafana/provisioning
    labels:
      io.rancher.scheduler.affinity:host_label: monitoramento=true
      io.rancher.container.agent.role: environment
      io.rancher.container.create_agent: 'true'
      io.rancher.container.pull_image: always
    environment:
      GF_INSTALL_PLUGINS: camptocamp-prometheus-alertmanager-datasource
```
Como o grafana não vem com o plugin do Alertmanager instalado por default, optei por realizar a instalação direto por uma variável de ambiente:

```yml
GF_INSTALL_PLUGINS: camptocamp-prometheus-alertmanager-datasource
```

O arquivo ``defaults.ini`` possui as configurações iniciais de inicialização do Grafana. Para esse desafio precisei modificar apenas a linha que comporta a variável ``root_url``:

Default:
```ini
root_url = %(protocol)s://%(domain)s:%(http_port)s
```

Após modificação:
```ini
root_url = %(protocol)s://%(domain)s:%(http_port)s/grafana/
```

Dessa forma, todos os requests que o grafana precisa para seu funcionamento passarão pelo caminho **/grafana/**. Isto permite que eu realize os desvios de HTTP para HTTPS pelo balancer sem precisar expor nenhuma porta.

Foi criado também um dashboard para visualização de consumo de memória, tráfego de rede, disco e CPU:

![image](https://i.ibb.co/Rzy44fK/image.png)
![image](https://i.ibb.co/c3nh3YY/image.png)

Para visualizar o dashboard completo clique em [devops.desafio-bry.tk/grafana](https://devops.desafio-bry.tk/grafana)
*  **Usuário**: admin
*  **Senha**: admin

> Para maiores informações sobre a implementação dessa aplicação, clique [aqui](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/tree/master/monitoramento/grafana).

### Alertmanager
O Alertmanager é o encarregado de manipular os alertas das aplicações configuradas no Prometheus.

Utilizei a versão mais recente da imagem ``prom/alertmanager:latest`` e externalizei o volume que comporta o arquivo de configuração para que fosse possível utilzar a configuração criada por mim:

```yml
 alertmanager:
    image: prom/alertmanager:latest
    stdin_open: true
    volumes:
    - /alertmanager:/etc/alertmanager
    tty: true
    command:
    - --web.external-url=https://devops.desafio-bry.tk/alertmanager
    - --config.file=/etc/alertmanager/alertmanager.yml
    - --storage.path=/etc/alertmanager/data
    labels:
      io.rancher.scheduler.affinity:host_label: monitoramento=true
      io.rancher.container.agent.role: environment
      io.rancher.container.create_agent: 'true'
      io.rancher.container.pull_image: always
```

No arquivo de configuração [alertmanager.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/monitoramento/alertmanager/alertmanager.yml) criei uma regra de inibição que permite silenciar um conjunto de alertas caso o alerta de nível crítico já esteja sendo notificado na instância, assim como utilizamos no meu trabalho atual:

```yml
inhibit_rules:
- source_match:
    severity: 'critical'
  target_match:
    severity: 'high'
  equal: ['alertname', 'instance']
  ```

Para acessar o Alertmanager clique em [devops.desafio-bry.tk/alertmanager](https://devops.desafio-bry.tk/alertmanager/).

> Para maiores informações sobre a implementação dessa aplicação clique [aqui](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/tree/master/monitoramento/alertmanager).

## balancer

Para o desafio, utilizei o HA-Proxy disponibilizado pelo Rancher para distribuir o tráfego da rede e das aplicações a partir dos containers e externalizei as portas 80 (HTTP) e 443 (HTTPS). 

```yml
version: '2'
services:
  haproxy:
    image: rancher/lb-service-haproxy:v0.9.13
    ports:
    - 443:443/tcp
    - 80:80/tcp
    labels:
      io.rancher.container.agent.role: environmentAdmin,agent
      io.rancher.container.agent_service.drain_provider: 'true'
      io.rancher.container.create_agent: 'true'
```

Para distribuir o tráfego, basta criar entradas no HA-Proxy que redirecionem as portas das aplicações para a porta 443 do HA-Proxy quando o contexto especificado é chamado.

Desta forma, torna-se desnecessário ter as portas das aplicações abertas externamente, criando uma proteção para acessos indevidos:

![image](https://i.ibb.co/Jxy1W9W/image.png)

*  Acessando [https://devops.desafio-bry.tk/whoami](https://devops.desafio-bry.tk/whoami) somos redirecionados para o target ``whoami/whoami`` na porta **8000**
*  Acessando [https://devops.desafio-bry.tk/prometheus](https://devops.desafio-bry.tk/prometheus) somos redirecionamos para o target ``monitoramento/prometheus`` na porta **9090**
*  Acessando [https://devops.desafio-bry.tk/metrics](https://devops.desafio-bry.tk/metrics) somos redirecionados para o target ``monitoramento/exporter`` na porta **9100**
*  Acessando [https://devops.desafio-bry.tk/grafana](https://devops.desafio-bry.tk/grafana) somos redirecionados para o target ``monitoramento/grafana`` na porta **3000** , utilizando o ``grafana_backend``
*  Acessando [https://devops.desafio-bry.tk/alertmanager](https://devops.desafio-bry.tk/alertmanager) somos redirecionados para o target ``monitoramento/alertmanager`` na porta **9093**

O target ``letsencrypt/letsencrypt`` é acessado e validado pelo HTTP Challenge citado acima.

O bloco abaixo mostra a configuração do haproxy.cfg mostrada na documentação oficial do [Grafana](https://grafana.com/docs/grafana/latest/installation/behind_proxy/) 

```yml
        frontend http-in
                          bind *:80
                          use_backend grafana_backend if { path /grafana } or { path_beg /grafana/ }
                        backend grafana_backend # configurado conforme documentação do Grafana
                          http-request set-path %[path,regsub(^/grafana/?,/)]

```

Adicionei também outro controle de acesso para que todo request HTTP seja redirecionado para a porta HTTPS:

```yml
        acl http      ssl_fc,not
        http-request redirect scheme https if http
```

## Gitlab CI

Optei por dividir o deploy automático em quatro estágios, um para cada stack:

```yml
stages:
  - deploy-whoami
  - deploy-letsencrypt
  - deploy-monitoramento
  - deploy-balancer
  ```
  Dessa forma podemos deixar o CI mais organizado colocando condições que executem determinados estágios quando arquivos específicos forem atualizados, garantindo que todas as stacks não sejam recriadas toda vez que um commit é realizado.
  
  Aqui teremos três variáveis protegidas:
  *  **ENV_RANCHER_V1_ACCESS_KEY** : valor da Access Key (Username) do Rancher
  *  **ENV_RANCHER_V1_SECRET_KEY_ENV** : valor da Secret Key (Password) do Rancher
  *  **ENV_RANCHER_V1_URL** : valor da URL do Rancher 1.6
  

Como mostrado anteriormente, essas variáveis são necessárias para realizar a autenticação via API do Rancher, possibilitando fazer deploys automáticos das stacks via CI.

Para os stages, utilizei a versão mais recente da imagem ``tagip/rancher-cli``. Com o Rancher CLI podemos executar os comandos do Rancher que nos permite deployar uma stack inteira utilizando os arquivos ``docker-compose`` e ``rancher-compose``.

Todos os stages seguem o mesmo raciocínio, portanto farei a explicação do ``deploy-whoami`` que englobará todos os outros, tornando a documentação menos maçante.

### deploy-whoami

Nesse estágio é realizado o deploy da stack ``whoami`` que contem a nossa aplicação web:

```yml
deploy:whoami:
  stage: deploy-whoami
  image: tagip/rancher-cli
  variables:
    RANCHER_URL: $ENV_RANCHER_V1_URL
    RANCHER_ACCESS_KEY: $ENV_RANCHER_V1_ACCESS_KEY
    RANCHER_SECRET_KEY: $ENV_RANCHER_V1_SECRET_KEY_ENV
  only:
    changes:
      - .gitlab-ci.yml
      - whoami/whoami-docker-compose.yml
      - whoami/whoami-rancher-compose.yml
  script:
    - rancher --debug up -s whoami -d -f ./whoami/whoami-docker-compose.yml --rancher-file ./whoami/whoami-rancher-compose.yml -p -u -c
```
Passamos as variáveis para autenticação via API do Rancher, os arquivos que podem ser modificados para que o ``deploy-whoami`` seja executado e o comando:
```yml
- rancher --debug up -s whoami -d -f ./whoami/whoami-docker-compose.yml --rancher-file ./whoami/whoami-rancher-compose.yml -p -u -c
```
Esse comando criará uma stack no Rancher com o nome ``whoami`` e fará a leitura dos arquivos [whoami-docker-compose.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/whoami/whoami-docker-compose.yml) e [whoami-rancher-compose.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/whoami/whoami-rancher-compose.yml) criando o container especificado e suas configurações no Rancher.

### deploy:letsencrypt

Arquivos:
*  [letsencrypt-docker-compose.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/letsencrypt/letsencrypt-docker-compose.yml)
*  [letsencrypt-rancher-compose.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/letsencrypt/letsencrypt-rancher-compose.yml)

### deploy:monitoramento
Arquivos:
*  [monitoramento-docker-compose.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/monitoramento/monitoramento-docker-compose.yml)
*  [monitoramento-rancher-compose.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/monitoramento/monitoramento-rancher-compose.yml)

### deploy:balancer
Arquivos:
*  [balancer-docker-compose.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/balancer/balancer-docker-compose.yml)
*  [balancer-rancher-compose.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/balancer/balancer-rancher-compose.yml)

> Para acessar o arquivo Gitlab CI completo clique [aqui](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/.gitlab-ci.yml).

## Trabalhos futuros

### Mais máquinas

Para esse desafio foi utilizado apenas uma máquina devido a problemas ao criar mais de um IP estático na GCP, o ideal seria termos no mínimo três máquinas: uma para provisionar as stacks ``balancer``, ``letsencrypt`` e ``whoami``, outra para comportar outro container da aplicação ``whoami`` e outras aplicações que viessem a aparecer, e a terceira para provisionar a stack ``monitoramento`` que acaba utilizando mais recursos.

Acredito que esse problema ocorra por estar usando uma conta para testes. Futuramente, pretendo procurar soluções para esse problema e tornar esse ambiente mais próximo a um ambiente de produção.

### Arquivos de configuração

Arquivos de configuração das aplicações : [alertmanager.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/monitoramento/alertmanager/alertmanager.yml), [defaults.ini](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/monitoramento/grafana/conf/defaults.ini), [node.json](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/monitoramento/grafana/provisioning/dashboards/node.json), [datasources/alertmanager.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/monitoramento/grafana/provisioning/datasources/alertmanager.yml), [datasources/prometheus.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/monitoramento/grafana/provisioning/datasources/prometheus.yml) e [prometheus.yml](https://gitlab.com/roberto.rvs/desafio-bry-aplicacoes/blob/master/monitoramento/prometheus/prometheus.yml) foram colocados manualmente via ssh para o servidor nas pastas externalizadas.

Como próximo passo, pretendo procurar uma solução para adicionar esses arquivos na máquina de forma automática, podendo ser na execução do CI ou na geração de novas imagens contendo os arquivos.


### Centralizador de Logs

Para centralizar os logs, experimentei a ferramenta [Loki](https://grafana.com/docs/grafana/latest/features/datasources/loki/#configure-the-data-source-with-provisioning), porém, não obtive sucesso.

Para que o loki consiga fazer a leitura dos logs dos containers, é necessário fazer a configuração do [Docker Logging Driver Plugin](https://grafana.com/blog/2019/07/15/lokis-path-to-ga-docker-logging-driver-plugin-support-for-systemd/) e adicionar as seguintes linhas no compose para cada container:

```yml
    logging:
      driver: loki
      options:
        loki-retries: "5"
        loki-batch-size: "400"
        loki-url: "https://admin:admin@logs-us-west1.grafana.net/loki/api/v1/push"
```

Entretanto, mesmo criando o container do Loki e adicionando as linhas de configuração para cada container, o compose não conseguiu fazer a leitura dessas linhas e direcionar os logs para o driver do loki.

Portanto, optei por não colocá-lo. Futuramente, pretendo procurar e desenvolver meus conhecimentos sobre centralizadores de logs e implementar alguma solução que satisfaça esse desafio, como por exemplo, o [Graylog](https://www.graylog.org/).

  
  